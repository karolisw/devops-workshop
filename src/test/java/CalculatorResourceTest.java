import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate() throws Exception {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100 + 300 + 100";
        assertEquals(500, calculatorResource.calculate(expression));

        expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 - 1 ";
        assertEquals(200, calculatorResource.calculate(expression));

        expression = " 300 - 10 ";
        assertEquals(290, calculatorResource.calculate(expression));

        expression = "10*3*2";
        assertEquals(60, calculatorResource.calculate(expression));

        expression = "10*3";
        assertEquals(30, calculatorResource.calculate(expression));

        expression = "20/2";
        assertEquals(10, calculatorResource.calculate(expression));

        expression = "20/2/2";
        assertEquals(5, calculatorResource.calculate(expression));
    }



    //Made changes to this test (changed expected value)
    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "300+99+ 1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    //Made changes to this test (changed expected value)
    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "20-2- 3";
        assertEquals(15, calculatorResource.subtraction(expression));

    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999*10";
        assertEquals(9990, calculatorResource.multiplication(expression));

        expression = "20*2";
        assertEquals(40, calculatorResource.multiplication(expression));

        expression = "20*2*3";
        assertEquals(120, calculatorResource.multiplication(expression));

        expression = "20*2 *2";
        assertEquals(80, calculatorResource.multiplication(expression));
    }
    @Test
    public void testDivision() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "90/10";
        assertEquals(9, calculatorResource.division(expression));
            
        expression = "20/2";
        assertEquals(10, calculatorResource.division(expression));

        expression = "20/2/2";
        assertEquals(5, calculatorResource.division(expression));

        expression = "20/2 /5";
        assertEquals(2, calculatorResource.division(expression));
    }
}
